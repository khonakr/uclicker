Project installation (localhost)

• Make sure to have a working Node.js installation

• Clone the project files from BitBucket repository here: 
>git clone https://khonakr@bitbucket.org/khonakr/uclicker.git 

• Change line 3 in db.js to point to the mongodb url (for database connectivity)

• Change line 41 in app.js to point to the mongodb url

• Change the university domain at line  62 of /config/passport.js to enable other domain names (default @uic.edu)

• Change the email credentials at line 24 and line 25 of /routes/index.js

• For any missing dependencies, cd to the directory that contains package.json, and type:
>npm install

• In the Node.js command prompt, cd to the project directory and run command
>node app.js

• The application should run on localhost:3000

• By default the application runs on port: 3000 , you may change that in line 18 of app.js

• Working project can be uploaded to Node.js supported host